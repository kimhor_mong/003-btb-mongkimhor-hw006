setInterval(() => {
    var options = {
        weekday: 'short',
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: true
    };
    document.getElementById('datetime').innerHTML = new Date().toLocaleTimeString('en-US', options);
}, 1000);


let btn = document.getElementById('btn');

/*put this inside onclick won't work*/
let start = 0;
let end = 0;
let total = 0;

btn.onclick = () => {
    let startedTime = document.getElementById('started-time');
    let stoppedTime = document.getElementById('stopped-time');
    let totalMinute = document.getElementById('total-minute');
    let payment = document.getElementById('payment');


    // date value for testing functionality
    // let startedDate = new Date('2000-01-01 07:00:00');
    // let endedDate = new Date('2000-01-01 08:00:00');

    if (btn.classList.contains('btn-success')) {
        // change button style
        btn.classList.replace('btn-success', 'btn-danger');
        btn.innerHTML = getBtnInnerStopElement();

        // store and set started time
        start = new Date(); // startedDate  
        startedTime.innerHTML = getHourWithAMPM(start);

    } else if (btn.classList.contains('btn-danger')) {
        // change button style
        btn.classList.replace('btn-danger', 'btn-warning')
        btn.innerHTML = getBtnInnerClearElement();

        // store and set stopped time
        end = new Date(); // endedDate
        stoppedTime.innerHTML = getHourWithAMPM(end);

        // get total minute - divided by 60000 to get total minute
        total = Math.floor((end - start) / 60000)
        // display total minute
        totalMinute.innerHTML = total;

        /* accumulate Payment */

        // 31-60 minutes = 1500 riels
        let oneHour = Math.floor(total / 60);
        total = total % 60;
        let thirtyOneMin = Math.floor(total / 31);
        total = total % 31;

        // 16 - 30 minutes = 1000 riels
        let thirtyMin = Math.floor(total / 30);
        total = total % 30;
        let sixtheenMin = Math.floor(total / 16);
        total = total % 16;

        // 0 - 15 minutes = 500 riels
        // if total > 0, it means total is between 1 and 15
        let fifteenMin = 0;
        if (total > 0) fifteenMin = 1;


        /* calculate total payment */

        let oneHourRange = 1500;
        let thirtyMinRange = 1000;
        let fifteenMinRange = 500;

        let totalPayment = ((oneHour + thirtyOneMin) * oneHourRange) +
            ((thirtyMin + sixtheenMin) * thirtyMinRange) +
            (fifteenMin * fifteenMinRange);

        // display total payment
        payment.innerHTML = totalPayment;

    } else if (btn.classList.contains('btn-warning')) {
        // change button style
        btn.classList.replace('btn-warning', 'btn-success')
        btn.innerHTML = getBtnInnerStartElement();

        // set started time, stopped itme, totalMinute, and payment to default
        stoppedTime.innerHTML = startedTime.innerHTML = '0:00';
        totalMinute.innerHTML = payment.innerHTML = '0';
    }
}

function getHourWithAMPM(date) {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    let strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function getBtnInnerStartElement() {
    return `<i class="fas fa-play fa-lg me-2"></i>
            <span>Start</span>`;
}

function getBtnInnerStopElement() {
    return `<i class="fas fa-stop-circle fa-lg me-2" style="color: white; background: radial-gradient(red 50%, transparent 50%);"></i>
            <span>Stop</span>`;
}

function getBtnInnerClearElement() {
    return `<i class="far fa-trash-alt fa-lg me-2" ></i>
                        <span>Clear</span>`;
}